require 'tempfile'
require 'csv'

def lift_over(chrpos, chain)
    # Setup
    # TODO: check src/* are exist
    binary = 'src/liftOver'

    # TODO: map chain to src/*.chain
    chain = 'src/hg18ToHg19.over.chain'

    old_file = Tempfile.open(['old_file', 'bed'])
    new_file = Tempfile.open(['new_file', 'bed'])
    unmapped = Tempfile.open(['unmapped', 'bed'])

    begin
        # Build query as BED file
        old_chr = chrpos.slice(0, 2).to_i
        old_pos = chrpos.slice(2, 12).to_i
        CSV.open(old_file, "w", :col_sep => "\t") do |csv|
            csv << ["chr#{old_chr}", old_pos - 1, old_pos, "xxx"]
	end

        # Execute liftOver command
        cmd = "#{binary} #{old_file.path} #{chain} #{new_file.path} #{unmapped.path}"
        retval = `#{cmd}`

        # TODO: check unmapped

	record = new_file.first.split("\t")
	new_chr = record[0].sub(/chr/, "").rjust(2, "0")
	new_pos = record[2].rjust(9, "0")
        new_chrpos = new_chr + new_pos
    ensure
        old_file.close!
        new_file.close!
        unmapped.close!
    end

    new_chrpos
end

p lift_over("01000743268", "hg18to19")